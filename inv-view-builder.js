/*global $, document, console */
/*jslint plusplus: true */

var inv = inv || {};

inv.TemplateBuilder = function () {
    'use strict';
    this.parseDom = function (element, scope) {
        var text = '', domElement = new inv.DomElement(element), attrMap, j, item, children, i, attr;
        var currentScope = scope;
        if (domElement.isElementNode()) {
            text += "<" + domElement.getTagName();
            if (domElement.hasAttribute("datasource")) {
                var scopeDataPropertyName = domElement.getAttributeValue("datasource");
                currentScope = scope.getPropertyScope(scopeDataPropertyName);
            } else if (domElement.hasAttribute("listsource")) {
                var scopeListPropertyName = domElement.getAttributeValue("listsource");
                currentScope = scope.getPropertyScope(scopeListPropertyName);
            }
            
            attrMap = domElement.getAttributes();
            for (attr in attrMap) {
                if (attrMap.hasOwnProperty(attr)) {
                    text += " " + attr + " = '" + attrMap[attr] + "'";
                }
            }
            //text += ' <br/>' + domElement.getHtml() || '';
        } else if (domElement.isTextNode()) {
            //console.log('text');
            //text += ' <br/> Text:' + domElement.getText() || '';
            text += this.replaceText(domElement.getText(), currentScope);
        }
        //$('#status').text('<span>textValue is:' + text + '</span>');
        children = domElement.getChildNodes();
        if (domElement.isElementNode() && children.length === 0) {
            text += "/>";
        } else if (domElement.isElementNode() && children.length > 0) {
            text += ">";
        }
        console.log('child length:' + children.length);
        for (i = 0; i < children.length; i++) {
            text += this.parseDom(children[i], scope);
        }
        
        if (domElement.isElementNode && children.length > 0) {
            text += "</" + domElement.getTagName() + ">";
        }
        
        return text;
    };
    
    this.replaceText = function (text, scope) {
        var regex = /[{]{2}([a-zA-Z0-9._]*)[}]{2}/g;
        function getReplacement(scope) {
            return function (match, p1, offset, string) {
                return scope.getValue(p1);
            };
        }
        var replacedText = text.replace(regex, getReplacement(scope));
        return replacedText;
    };
};

inv.DataScope = function (scopeValueParam) {
    'use strict';
    var scopeValue = scopeValueParam;
    ///
    this.getPropertyScope = function (propertyName) {
        if (propertyName == null || propertyName === "") {
            return new inv.DataScope(scopeValue);
        }
        var propertyValue = this.getProperty(propertyName),
            newScope = new inv.DataScope(propertyValue);
        return newScope;
    };
    this.getValue = function (propertyName) {
        if (!propertyName) {
            return scopeValue;
        }
        return scopeValue[propertyName];
    };
    this.getProperty = function (propertyName) {
        return scopeValue[propertyName];
    };
    this.IsArrayType = function (propertyName) {
        var propertyValue = this.getProperty(propertyName);
        return propertyValue instanceof Array;
    };
};


inv.DomElement = function (element) {
    'use strict';
    var domElement = $(element);
    this.isElementNode = function () {
        console.log(domElement.get(0).nodeType);
        return domElement.get(0).nodeType === 1;
    };
  
    this.isTextNode = function () {
        return domElement.get(0).nodeType === 3;
    };
  
    this.getChildNodes = function () {
        return domElement.contents();
    };
  
    this.getHtml = function () {
        return domElement.html();
    };
  
    this.getText = function () {
        return domElement.text();
    };
  
    this.getAttributes = function () {
        var attrs = domElement.get(0).attributes, attrMap = {}, j, item;
        for (j = 0; j < attrs.length; j++) {
            item = attrs[j];
            attrMap[item.name] = item.value;
        }
        return attrMap;
    };
    
    this.hasAttribute = function (propertyName) {
        var propertyValue = domElement.attr(propertyName);
        if (propertyValue == null) {
            return false;
        }
        return true;
    };
    
    /**
    * @name getAttributeValue
    * @description gets the attribute value given the property name    
    */
    this.getAttributeValue = function (propertyName) {
        return domElement.attr(propertyName);
    };
    
    this.getTagName = function () {
        return domElement.prop('tagName').toLowerCase();
    };
};
