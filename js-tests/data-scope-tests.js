/*global describe, it, inv, expect */
/*jslint plusplus:true*/

describe('Scope value', function () {
    'use strict';
    var json = {
        "Property1" : "Value1",
        "Property2" : "Value2"
    };
    
    var complexJson = {
        "Name" : "Arun",
        "Address" : {
            "City" : "Connoquenessing"
        }
    };
    
    
    it('should return the scope object when empty', function () {
        var scope = new inv.DataScope(json);
        var value = scope.getValue();
        expect(value).toEqual(json);
    });
    
    it('should return the value of the property when property name is specified', function () {
        var scope = new inv.DataScope(json);
        var value = scope.getValue("Property1");
        expect(value).toBe("Value1");
        
        var value2 = scope.getValue("Property2");
        expect(value2).toBe("Value2");
    });
    
    it('should return the value of the complex property when the complex property name is specified', function () {
        var expectedValue = {
            "City" : "Connoquenessing"
        },
            scope = new inv.DataScope(complexJson),
            value = scope.getValue("Address");
        expect(value).toEqual(expectedValue);
    });
    
    it('should be scoped at the same object level if empty property name was specified', function () {
        var expectedValue = {
            "City" : "Connoquenessing"
        },
            scope = new inv.DataScope(complexJson),
            newScope = scope.getPropertyScope(""),
            value = newScope.getValue("Address");
            expect(value).toEqual(expectedValue);
    });
});