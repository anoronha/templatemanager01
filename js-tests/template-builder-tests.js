/*global describe, it, expect, inv, $, console*/
describe('template builder functionality', function () {
    'use strict';
    it('should print values', function () {
        var templateBuilder = new inv.TemplateBuilder(),
            templateText = $('#templateSection').html(),
            element = $(templateText),
            json = {"propertysource": "one"},
            scope = new inv.DataScope(json),
            result;
        result = templateBuilder.parseDom(element, scope);
        console.log(element);
        console.log(result);
        $('textarea').val(result);
    });
});

describe('template builder replace functionality', function () {
    'use strict';
    it('should replace placeholder only text with values', function () {
        var templateBuilder = new inv.TemplateBuilder(), scope = new inv.DataScope({"propone": "one"}),
            result = templateBuilder.replaceText("{{propone}}", scope);
        expect(result).toBe("one");
    });
    
    it('should replace placeholder ending text with values', function () {
        var templateBuilder = new inv.TemplateBuilder(), scope = new inv.DataScope({"propone": "one"}),
            result = templateBuilder.replaceText("This is {{propone}}", scope);
        expect(result).toBe("This is one");
    });
    
    it('should replace placeholder starting text with values', function () {
        var templateBuilder = new inv.TemplateBuilder(), scope = new inv.DataScope({"propone": "one"}),
            result = templateBuilder.replaceText("{{propone}} more thing", scope);
        expect(result).toBe("one more thing");
    });
    
    it('should replace placeholder containing text with values', function () {
        var templateBuilder = new inv.TemplateBuilder(), scope = new inv.DataScope({"propone": "one"}),
            result = templateBuilder.replaceText("zero {{propone}} two", scope);
        expect(result).toBe("zero one two");
    });
    
    it('should replace placeholder containing text with values', function () {
        var templateBuilder = new inv.TemplateBuilder(), scope = new inv.DataScope({"propone": "one", "propthree": "three", "propfive": "five"}),
            result = templateBuilder.replaceText("{{propone}} two {{propthree}} four {{propfive}}", scope);
        expect(result).toBe("one two three four five");
    });
});