/*global describe, it, expect, console, $*/
/*jslint plusplus:true*/
describe('javascript tests', function () {
    'use strict';
    var json = {
            "propTask" : "Task",
            "propDone" : "Done"
        };
    describe('getting a type of array', function () {
        it('should return successful if the value is a array', function () {
            var arr = [];
            expect(arr instanceof Array).toBe(true);
        });
    });
    
    describe('regular expression matching', function () {
        it('should have multiple matches on text that contain multiple placeholders', function () {
            var text = 'this is a {{propTask}} that should be {{propDone}}',
                regex = /[{]{2}([a-zA-Z0-9._]*)[}]{2}/g,
                //matches = text.match(regex),
                //match,
                //i,
                newtext;
//            console.log(matches);
//            console.log(matches.length);
//            for (i = 0; i < matches.length; i++) {
//                match = matches[i];
//                console.log(match[2]);
//                console.log(match);
//            }
//            console.log(json);
//            var self = this;
            newtext = text.replace(regex, getReplacement(json));
            console.log(newtext);
            
            expect(newtext).toBe('this is a Task that should be Done');
        });
    });
    
    function getReplacement(json) {
        console.log(json);
        return function (match, p1, offset, string) {
            return json[p1];
        };
    }
});



function replaceText(text, json) {
    'use strict';
    var regex = /[{]{2}[a-zA-Z0-9._]*[}]{2}/;
    
}