/*global describe, it, expect, inv, $*/

describe('dom element behavior', function () {
    'use strict';
    var element = $('#element01');
    var domElement = new inv.DomElement(element);
        
    it('a div element should get the right tagName', function () {
        var tagName = domElement.getTagName();
        expect(tagName).toBe("div");
    });
    
    it('an element with datsource should get the right datasource propertyName', function () {
        var doesDatasourceExist = domElement.hasAttribute('datasource');
        var datasourceValue = domElement.getAttributeValue('datasource');
        expect(doesDatasourceExist).toBe(true);
        expect(datasourceValue).toBe('propertysource');
    });
});